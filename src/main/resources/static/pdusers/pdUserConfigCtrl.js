angular.module("lamsAdmin").controller("pdUserConfigCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification", "NgTableParams", "$filter",
	function($scope, $http, $rootScope, Constant, userService, Notification,NgTableParams, $filter) {

		$scope.forms = {};
		$scope.users = [];
		$scope.resetUser = function(){
			$scope.userData = {};
		}
		
		$scope.agenciesTypes = [ "PD User","FCI User" ];
		$scope.currentCaptionAgencyType='PD User';
		$scope.agencyType = "PD User";
		$scope.userTypeAgencyTypeId=Constant.UserType.PD_USER.id;
		
		$scope.toggleAgenciesTypes = function(status) {
			$scope.currentCaptionAgencyType=status;
			
			if(status == 'PD User'){
				$scope.userTypeAgencyTypeId=Constant.UserType.PD_USER.id;
				$scope.getUsersConfig(Constant.UserType.PD_USER.id);
				$scope.getQuestions(Constant.UserType.PD_USER.id);
			}
			else if(status == 'FCI User'){
				$scope.userTypeAgencyTypeId=Constant.UserType.FCI_USER.id;
				$scope.getUsersConfig(Constant.UserType.FCI_USER.id);
				$scope.getQuestions(Constant.UserType.FCI_USER.id);
			}
		};
		
		$scope.getUsersConfig = function(userType) {
			//userType is NUll then will fetch all the users
			userService.getUserConfigByType(userType).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.userData = success.data.data;
						console.log("$scope.users==>", $scope.users);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getUsersConfig(Constant.UserType.PD_USER.id);
		
		$scope.addMore = function(){
			if($scope.allQuestions == null){
				$scope.allQuestions=[];
			}
			$scope.allQuestions.push({"questionDesc" : null});
		}
		
		$scope.getQuestions = function(userType) {
			//userType is NUll then will fetch all the users
			userService.getQuestions(userType).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.allQuestions = success.data.data;
						console.log("$scope.users==>", $scope.users);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getQuestions(Constant.UserType.PD_USER.id);

		$scope.editUserData = function(user) {
			$scope.userData = angular.copy(user);
			$scope.showEditMode = true;
			$scope.userData.password = $scope.userData.tempPassword;
			$scope.userData.confirmPassword = $scope.userData.tempPassword;
			if(!$rootScope.isEmpty($scope.userData.applications)){
				$scope.userData.applicationTypeId = $scope.userData.applications[0].applicationTypeId;				
			}
			$("#userEmail").focus();
		}

		$scope.updateUserConfigurationDetail = function() {
			$scope.userData.userType = $scope.userTypeAgencyTypeId;
			$scope.userData.agencyTypeId = $scope.userTypeAgencyTypeId;
			console.log("$scope.userData===>",$scope.userData);
			userService.updateUserConfigurationDetail($scope.userData).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.success(success.data.message);
						//$scope.resetUser();
						$scope.getUsers($scope.userTypeAgencyTypeId);
					} else if (success.data.status == 400) {
						Notification.warning(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}

		$scope.saveInterviewQuestions = function() {
			var questionData = {};
			questionData.allQuestionsMstr = [];
			questionData.agencyType=$scope.userTypeAgencyTypeId;
			
			$scope.allQuestions.forEach(function (row) {
				questionData.allQuestionsMstr.push(row);
	        });
			
			questionData.data = JSON.stringify(questionData.allQuestionsMstr);
			
			userService.updateInterviewQuestionsDetail(questionData).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.success(success.data.message);
						//$scope.resetUser();
						$scope.getUsers($scope.userTypeAgencyTypeId);
					} else if (success.data.status == 400) {
						Notification.warning(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.search = {};
		$scope.$watch("search.pduser", function () {
            $scope.userTable.reload();
            $scope.userTable.page(1);
        });
		
		$scope.userTable = new NgTableParams({page: 1, count: 500, sorting: {firstName: "asc"}}, {
            counts: [],
            getData: function ($defer, params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.users, params.orderBy()) : $scope.users;
                if ($scope.search.pduser) {
                    orderedData = $filter('filter')(orderedData, $scope.search.pduser);
                }
                if (orderedData) {
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            }
        });

	} ]);