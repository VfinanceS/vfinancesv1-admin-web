angular.module("lamsAdmin").controller("pdUsermgmtCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification", "NgTableParams", "$filter",
	function($scope, $http, $rootScope, Constant, userService, Notification,NgTableParams, $filter) {

		$scope.forms = {};
		$scope.users = [];
		$scope.resetUser = function(){
			$scope.userData = {bank : {}};
		}
		
		$scope.agenciesTypes = [ "PD User","FCI User" ];
		$scope.currentCaptionAgencyType='PD User';
		$scope.agencyType = "PD User";
		$scope.userTypeAgencyTypeId=Constant.UserType.PD_USER.id;
		
		$scope.toggleAgenciesTypes = function(status) {
			$scope.currentCaptionAgencyType=status;
			
			if(status == 'PD User'){
				$scope.userTypeAgencyTypeId=Constant.UserType.PD_USER.id;
				$scope.getUsers(Constant.UserType.PD_USER.id);
			}
			else if(status == 'FCI User'){
				$scope.userTypeAgencyTypeId=Constant.UserType.FCI_USER.id;
				$scope.getUsers(Constant.UserType.FCI_USER.id);
			}
		};
		
		$scope.getUsers = function(userType) {
			//userType is NUll then will fetch all the users
			userService.getUserByType(userType).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.users = success.data.data;
						$scope.userTable.reload();
	                    $scope.userTable.page(1);						
						console.log("$scope.users==>", $scope.users);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getUsers(Constant.UserType.PD_USER.id);

		$scope.editUserData = function(user) {
			$scope.userData = angular.copy(user);
			$scope.showEditMode = true;
			$scope.userData.password = $scope.userData.tempPassword;
			$scope.userData.confirmPassword = $scope.userData.tempPassword;
			if(!$rootScope.isEmpty($scope.userData.applications)){
				$scope.userData.applicationTypeId = $scope.userData.applications[0].applicationTypeId;				
			}
			$("#userEmail").focus();
		}

		$scope.updatePDAgencyDetails = function() {
			if (!$scope.forms.pdAgencyForm.$valid) {
				$scope.forms.pdAgencyForm.$submitted = true;
				Notification.warning("Please fill all mandatory data");
				return false;
			}
			if ($scope.userData.password.trim() != $scope.userData.confirmPassword.trim()) {
				Notification.warning("Password and confirm passwod not matched!!");
				return false;
			}
			$scope.userData.userType = $scope.userTypeAgencyTypeId;
			$scope.userData.applications = [{"applicationTypeId" : $scope.userData.applicationTypeId}];
			$scope.userData.isProfileFilled = true;
			console.log("$scope.userData===>",$scope.userData);
			userService.updatePDAgencyDetails($scope.userData).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.success(success.data.message);
						$scope.resetUser();
						$scope.getUsers($scope.userTypeAgencyTypeId);
					} else if (success.data.status == 400) {
						Notification.warning(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}

		$scope.search = {};
		$scope.$watch("search.pduser", function () {
            $scope.userTable.reload();
            $scope.userTable.page(1);
        });
		
		$scope.userTable = new NgTableParams({page: 1, count: 500, sorting: {firstName: "asc"}}, {
            counts: [],
            getData: function ($defer, params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.users, params.orderBy()) : $scope.users;
                if ($scope.search.pduser) {
                    orderedData = $filter('filter')(orderedData, $scope.search.pduser);
                }
                if (orderedData) {
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            }
        });

		
		

	} ]);