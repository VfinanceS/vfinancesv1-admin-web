angular.module("lamsAdmin").controller("channelPartnerProfileCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification","$stateParams","$filter","payoutService", 
	function($scope, $http, $rootScope, Constant, userService, Notification, $stateParams, $filter, payoutService) {

	
	var cpId = $stateParams.cpId;
		userService.getUserDetailsById(cpId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		$scope.userData = success.data.data;
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });		
		
		$scope.getClient = function(userType,userId){
			userService.getCpUsers(userType,userId).then(
		            function(success) {
		            	console.log("success.data==============>",success.data);
		            	if(success.data.status == 200){
		            		$scope.borrowers = success.data.data; 
		                }else{
		                	Notification.warning(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });	
		}
		$scope.getClient(Constant.UserType.ALL.id,cpId);
		
		$scope.remove = function(selectedRow, parentInx, indx) {
			var newDataList = [];
			$scope.selectedAll = false;

			$scope.allPayOuts[parentInx].payOuts.splice(indx, 1);
			
			$scope.personalDetails = newDataList;
		};
		
		$scope.collapseAll = function() {
			$scope.IsAllCategoriesCollapsed = !$scope.IsAllCategoriesCollapsed;
		    $scope.allPayOuts.forEach(function(item) {
		    	item.isCollapsed = $scope.IsAllCollapsed;
		    })
		}
		
		$scope.expandAll = function(expanded) {
	        // $scope is required here, hence the injection above, even though we're using "controller as" syntax
	        $scope.$broadcast('onExpandAll', {
	          expanded: expanded
	        });
	      };

		$scope.$on('onExpandAll',function(event,args){
			$scope.expanded = args.expanded;
	    })
		
		$scope.allPayOuts = [];
		$scope.getPaymentDetails = function(userId){
			payoutService.getPaymentDetails(userId).then(
		            function(success) {
		            	console.log("success.data==============>",success.data);
		            	if(success.data.status == 200){
		            		$scope.allPayOuts = success.data.data; 
		                }else{
		                	Notification.warning(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });	
		}
		$scope.getPaymentDetails(cpId);
		
		$scope.cpCommisionsMonthYearWise = [];
		$scope.getCpCommisionCalculationList = function(cpId){
			payoutService.getCpCommisionCalculationList(cpId).then(
		            function(success) {
		            	console.log("success.data==============>",success.data);
		            	if(success.data.status == 200){
		            		$scope.cpCommisionsMonthYearWise = success.data.data; 
		                }else{
		                	Notification.warning(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });	
		}
		$scope.getCpCommisionCalculationList(cpId);
		
		$scope.addNew = function(index) {
			console.log("Index : "+index+" element : "+$scope.allPayOuts[0]);
			$scope.allPayOuts[index].payOuts.push({
				"id":null,
				"applicationTypeId":null,
				"disbursmentFromAmt":0,
				"disbursmentToAmt":0,
				"payOutInPerc":0,
				"version":null,
				"createdDate":null,
				"createdBy":null,
				"modifiedDate":null,
				"modifiedBy":null,
				"channelPartners":null
			});
		};
		
		$scope.savePayoutFor = function(index) {
			console.log("All rows : "+$scope.allPayOuts[index].payOuts);
			
			var applWithPayData = {};
			applWithPayData.id = $scope.allPayOuts[index].id;
			applWithPayData.code = $scope.allPayOuts[index].code;
			applWithPayData.name = $scope.allPayOuts[index].name;
			applWithPayData.applTypId = $scope.allPayOuts[index].applTypeId;
			applWithPayData.payOuts = [];
			
			$scope.allPayOuts[index].payOuts.forEach(function (row) {
				applWithPayData.payOuts.push(row);
	        });
			
			applWithPayData.data = JSON.stringify(applWithPayData);
			
			payoutService
			.savePayoutsForChnlPartner(applWithPayData, cpId)
			.then(
					function(success) {
						if (success.data.status == 200) {
							Notification.info("Payouts For Channel Partner Saved  Successfully!!");
						} else if (success.data.status == 400) {
							Notification.error(success.data.message);
						} else {
							Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
						}
					},
					function(error) {
						console.log("error==>>",error);
						$rootScope.validateErrorResponse(error);
					});
		};
	
}]);
