angular.module("lamsAdmin").controller("applicationCtrl", [ "$scope", "$rootScope", "Notification", "applicationService", "Constant", "$filter", "$stateParams", "documentService","userService",
	function($scope,  $rootScope, Notification, applicationService, Constant, $filter, $stateParams, documentService, userService) {

		$scope.loanCategoryList = [
			{id : 0, name : "New"},
			{id : 1, name : "Balance Transfer"}
		];
		
		$scope.popup ={"opened" : false};
		$scope.open = function() {
		    $scope.popup.opened = true;
		};
	
		$scope.applicationTypeCode = $stateParams.appCode;
		$scope.applicationTypeId = $rootScope.getAppTypeIdByCode($scope.applicationTypeCode);
		$scope.applicationId = $stateParams.appId;
		$scope.editApplicationForm = true;
		$scope.statuses = [ Constant.Status.RESPONDED, Constant.Status.ACCEPTED, Constant.Status.REJECTED,Constant.Status.FORMAPPLIED,Constant.Status.SANCTIONED,Constant.Status.DISBURSED ];
		$scope.status = Constant.Status.RESPONDED;
		$scope.employmentType = $stateParams.empType;
		$scope.disbursmentDate='';
		
		$scope.processSubmitFormDocuments={};
		$scope.processSactionedFormDocuments={};
		$scope.processDisbursedDocumentDocuments={};
		
		$scope.pdReportDocuments={};
		$scope.fciReportDocuments={};
		
		$scope.getApplicationDetails = function() {

			applicationService.getLoanDetails($scope.applicationId, $scope.applicationTypeId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.applicationDetails = success.data.data;
						
						if($scope.applicationDetails.coapplicants !=null && $scope.applicationDetails.coapplicants.length > 0){
							$scope.coApplicant = $scope.applicationDetails.coapplicants[0];
						}
						
						//Co-Applicant Documents
						if($scope.applicationDetails.coapplicants !=null && $scope.applicationDetails.coapplicants.length > 0){
							
							if ($scope.coApplicant.employmentType == Constant.EmploymentType.SALARIED) {
								$scope.getCoApplicantDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
									Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
									Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
							} else if ($scope.coApplicant.employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
								$scope.getCoApplicantDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
									Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
									Constant.documentType.CORPORATE_ITR_SET_YEAR3,
									Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
									Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
									Constant.documentType.INDIVIDUAL_BANK_ACCOUNT_STATEMENT ]);
							} else if ($scope.applicationDetails.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED || 
									$scope.coApplicant.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED) {
								$scope.getCoApplicantDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
							}
						}
						
						//Applicant Documents
						if ($scope.employmentType == Constant.EmploymentType.SALARIED) {
							$scope.getDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
								Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
						} else if ($scope.employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
							$scope.getDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
								Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
								Constant.documentType.CORPORATE_ITR_SET_YEAR3,
								Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
								Constant.documentType.INDIVIDUAL_BANK_ACCOUNT_STATEMENT ]);
						} else if ($scope.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED) {
							$scope.getDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
						}
						
						$scope.getDisbursmentList();
						$scope.getPDReportingDocumentList([ Constant.documentType.PDI_REPORT_DOCUMENT ]);
						$scope.getFCIReportingDocumentList([ Constant.documentType.FCI_REPORT_DOCUMENT ]);
						
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getApplicationDetails();

		

		$scope.documentList = [];
		$scope.getDocumentList = function(listOfDocumentMstId) {
			console.log("$scope.documentList---------------------------------->",$scope.documentList)
			documentService.getDocumentList($scope.applicationId, listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.documentList = success.data.data;
						console.log("$scope.documentList---------------------------------->",$scope.documentList)
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.disbursmentList = [];
		$scope.getDisbursmentList = function() {
			console.log("$scope.documentList---------------------------------->",$scope.documentList)
			applicationService.getDisbursmentList($scope.applicationId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.disbursmentList = success.data.data;
						console.log("$scope.documentList---------------------------------->",$scope.disbursmentList)
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.coApplicantDocumentList = [];
		$scope.getCoApplicantDocumentList = function(listOfDocumentMstId) {
			
			documentService.getCoApplicantsDocumentList($scope.applicationId, $scope.coApplicant.id, listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.coApplicantDocumentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.getConnections = function(appId, status) {
			applicationService.getConnections(appId, status).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.connections = success.data.data;
					} else {
						Notification.error(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};

		$scope.getConnections($scope.applicationId, Constant.Status.RESPONDED);

		applicationService.getDocumentsListForProcessing($scope.applicationId).then(
			function(success) {
				if (success.data.status == 200) {
					
					$scope.processSubmitFormDocuments=success.data.data[0];
					$scope.processSactionedFormDocuments=success.data.data[1];
					$scope.processDisbursedDocumentDocuments=success.data.data[2];
					
					if(typeof(processSubmitFormDocuments) != 'undefined' && ($scope.processSubmitFormDocuments.documentResponseList != null && $scope.processSubmitFormDocuments.documentResponseList.length > 0)){
						$scope.formSubmissionFileEdit = false;
					}
					
					if(typeof(processSactionedFormDocuments) != 'undefined' && ($scope.processSactionedFormDocuments.documentResponseList.length > 0)){
						$scope.sactionedFileEdit = false;
					}
				} else {
					Notification.error(success.data.message);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
		
		$scope.uploadAppFile = function(element) {
			$rootScope.uploadFile(element.files, $scope.applicationId, element.id, $scope,false);
		}
		
		$scope.updateStatus = function (){
			applicationService.updateStatus($scope.applicationId).then(
		            function(success) {
		            	if(success.data.status == 200){
		            		Notification.success("Application Status Changed!");
		            		//getUserDetailsById($scope.brId);
		            		$scope.applicationDetails.status = success.data.data.status;
		            		$scope.disbursmentList = success.data.data.loanTransactionList;
		            		$scope.saveButtonEdit = false;
		                }else{
		                	Notification.error(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });
		};
		
		$scope.autoUpdateStatus = function (){
			console.log($scope.processSubmitFormDocuments.documentResponseList);
			if($scope.processSubmitFormDocuments.documentResponseList.length == 0){
				Notification.error("Document upload mandatory");
			}
			console.log("test--"+$scope.disbursmentDate+" disbursmentDate"+disbursmentDate.value+" disbursmentAmount : "+disbursmentAmount.value);
			if($scope.applicationDetails.status == 'SANCTIONED' && disbursmentDate.value == '' && disbursmentAmount.value == ''){
				Notification.warning("All details are mandatory");
				return;
			}
			
			applicationService.disbursmentSaved($scope.applicationId, disbursmentDate.value, disbursmentAmount.value).then(
		            function(success) {
		            	if(success.data.status == 200){
		            		Notification.success("Application Status Changed!");
		            		if(success.data !=null && success.data.data !=null){
		            			//loanTransactionList
		            			//status
		            			$scope.applicationDetails.status = success.data.data.status;
			            		$scope.disbursmentList = success.data.data.loanTransactionList;
		            		}
		            		
		            		$scope.saveButtonEdit = false;
		                }else{
		                	Notification.error(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });
		};
		
	    $scope.getPDReportingDocumentList = function(listOfDocumentMstId) {
			documentService.getPDReportingDocumentsList($scope.applicationId, "PDI_REPORT", listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.pdReportDocuments = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
	     
	    $scope.getFCIReportingDocumentList = function(listOfDocumentMstId) {
			documentService.getPDReportingDocumentsList($scope.applicationId, "FCI_REPORT", listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.fciReportDocuments = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
	    
	    $scope.allQuestions=[];
		$scope.interviewStatus;
		$scope.getQuestions = function(appId) {
			//userType is NUll then will fetch all the users
			userService.getQuestionsAnswers(appId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.allQuestions = success.data.data.allQuestionsMstr;
						$scope.interviewStatus = success.data.data.markedInterviewDone;
						console.log("$scope.users==>", $scope.users);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getQuestions($scope.applicationId);

	} ]);