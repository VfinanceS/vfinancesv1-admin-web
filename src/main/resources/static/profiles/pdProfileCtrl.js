angular.module("lamsAdmin").controller("pdProfileCtrl",["$scope", "$http","$rootScope","Constant","userService","Notification","masterService","$filter","$stateParams","applicationService",
		function($scope, $http, $rootScope,Constant,userService,Notification,masterService,$filter,$stateParams,applicationService) {

	var ldId = $stateParams.ldId;
	$scope.isDisable = false;
	$scope.userData = {};
	$scope.connections = [];
	
	
	$scope.getConnections = function(id) {
		applicationService.getConnectionByLenderId(id).then(
			function(success) {
				if (success.data.status == 200) {
					console.log(success.data.data);
					$scope.connections = success.data.data;
				} else {
					Notification.error(success.data.message);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
	};
	$scope.getConnections(ldId);
	
	$scope.getUserDetail = function(id){
		userService.getUserDetailById(id).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		$scope.userData = success.data.data;
	            		console.log("$scope.userData==>",$scope.userData);
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });		
	}
	$scope.getUserDetail(ldId);
	
	$scope.updateUserConfigurationDetail = function() {
		$scope.userData.userType = $scope.userTypeAgencyTypeId;
		$scope.userData.agencyTypeId = $scope.userTypeAgencyTypeId;
		$scope.userData.userId = ldId;
		console.log("$scope.userData===>",$scope.userData);
		userService.updateUserConfigurationDetail($scope.userData).then(
			function(success) {
				if (success.data.status == 200) {
					Notification.success(success.data.message);
					$scope.getUsersConfig($scope.userTypeAgencyTypeId);
				} else if (success.data.status == 400) {
					Notification.warning(success.data.message);
				} else {
					Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
				}
			}, function(error) {
				console.log("error==>>", error);
				$rootScope.validateErrorResponse(error);
			});
	}
	
	$scope.applications = [];
	$scope.getApplications = function() {
		applicationService.getApplicationsForPDReports(ldId).then(
			function(success) {
				if (success.data.status == 200) {
					console.log(success.data.data);
					$scope.applications = success.data.data;
				} else {
					Notification.error(success.data.message);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
	};
	$scope.getApplications(ldId);
	
	$scope.getUsersConfig = function() {
		//userType is NUll then will fetch all the users
		userService.getUserConfigByUser(ldId).then(
			function(success) {
				if (success.data.status == 200) {
					$scope.userData = success.data.data;
					console.log("$scope.users==>", $scope.users);
				} else if (success.data.status == 400) {
					Notification.error(success.data.message);
				} else {
					Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
				}
			}, function(error) {
				console.log("error==>>", error);
				$rootScope.validateErrorResponse(error);
			});
	}
	$scope.getUsersConfig();
	
}]);
