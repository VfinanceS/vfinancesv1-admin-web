angular.module("lamsAdmin").controller(
				"borrowerProfileCtrl",
				[
						"$scope",
						"$http",
						"$rootScope",
						"Constant",
						"userService",
						"Notification",
						"$stateParams",
						"$filter",
						"$uibModal",
						"documentService",
						function($scope, $http, $rootScope, Constant, userService, Notification, $stateParams, $filter, $uibModal, documentService) {

							$scope.coApplicantUserId = {};
							var brId = $stateParams.brId;
							
							$scope.loanCategoryList = [
							               			{id : 0, name : "New"},
							               			{id : 1, name : "Balance Transfer"}
							               		];
							
							userService.getUserDetailsById(brId).then(
											function(success) {
												if (success.data.status == 200) {
													$scope.userData = success.data.data;
													$scope.existingAppCount = $filter('filter')($scope.userData.applications,{
																		loanTypeId : Constant.LoanType.EXISTING_LOAN
																	}).length;
													$scope.curentAppCount = $filter('filter')(
																	$scope.userData.applications,
																	{loanTypeId : Constant.LoanType.CURRENT_LOAN}).length;
													$scope.closedAppCount = $filter('filter')($scope.userData.applications,{
																		loanTypeId : Constant.LoanType.CLOSED_LOAN
																	}).length;
												} else {
													Notification.error(success.data.message);
												}
											},
											function(error) {
												$rootScope.validateErrorResponse(error);
											});
							
							$scope.brProfileImage = function() {
								documentService.getSelectedUserDocument(brId, Constant.documentType.PHOTO_GRAPH).then(
									function(success) {
										if (success.data.status == 200) {
											$scope.brProfileImageData = success.data.data;
										} else {
											Notification.warning(success.data.message);
										}
									}, function(error) {
										$rootScope.validateErrorResponse(error);
									});
							};
							$scope.brProfileImage();

							$scope.openModal = function() {
								console.log("Selected Borrower : "+ $scope.coApplicantUserId);
								
								if ($scope.coApplicantUserId != 5) {
									
									userService.getBrCoApplicantUserDetails($scope.coApplicantUserId).then(
											function(success) {
												if (success.data.status == 200) {
													$scope.brCoapplicantUserData = success.data.data;
													
													var modalInstance = $uibModal
																.open({
																	ariaLabelledBy : 'modal-title',
																	ariaDescribedBy : 'modal-body',
																	templateUrl : 'profiles/coApplicantProfile.html',
																	controller : 'coApplicantProfileCtrl',
																	controllerAs : '$ctrl',
																	scope: $scope,
																	size : 'lg',
																	resolve : {
																		coApplicantUserData: function () {
																			console.log("Resolve : "+$scope.brCoapplicantUserData)
																			return $scope.brCoapplicantUserData; 
																		}
																	}
																});
													
													modalInstance.result.then(function(brCoapplicantUserData) {
															console.log("Modal is dimissed ! "+brCoapplicantUserData);
															$scope.coApplicantUserData = brCoapplicantUserData;
														}, function(res) {
															// $scope.currentObj.coApplicantUserId={};
														});
												} else {
													Notification.error(success.data.message);
												}
											},
											function(error) {
												$rootScope.validateErrorResponse(error);
											});
									
								}
							}

						} ]);

angular.module("lamsAdmin").controller("coApplicantProfileCtrl", function($scope, $http, $rootScope, Constant, userService, Notification, masterService, $filter) {
	//console.log("Modal controller : "+coApplicnt)
});