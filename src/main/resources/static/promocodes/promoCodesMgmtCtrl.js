angular
		.module("lamsAdmin")
		.controller(
				"promoCodesMgmtCtrl",
				[
						"$scope",
						"$http",
						"$rootScope",
						"Constant",
						"userService",
						"Notification",
						"NgTableParams",
						"$filter",
						"promocodesService",
						function($scope, $http, $rootScope, Constant,
								userService, Notification, NgTableParams,
								$filter, promocodesService) {

							$scope.forms = {};
							$scope.users = [];
							$scope.resetPromoCode = function() {
								$scope.promoCodeData = {
									
								};
							}
							
							$scope.promoCodes = [];
							
							$scope.length;
							$scope.quantity;
							$scope.validDays;
							$scope.applicationTypeId;
							$scope.isActive;
							$scope.promoCode;
							$scope.promoCodeDescription;
							
							$scope.getPromoCodes = function() {
								promocodesService.getAllPromoCodes().then(
									function(success) {
										if (success.data.status == 200) {
											$scope.promoCodes = success.data.data;
											console.log("$scope.users==>", $scope.users);
										} else if (success.data.status == 400) {
											Notification.error(success.data.message);
										} else {
											Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
										}
									}, function(error) {
										console.log("error==>>", error);
										$rootScope.validateErrorResponse(error);
									});
							}
							$scope.getPromoCodes();

							$scope.editPromoCodeData = function(promoCode) {
								$scope.promoCodeData = angular.copy(promoCode);
								$scope.showEditMode = true;
								$scope.promoCodeData.applicationTypeId = $scope.promoCodeData.applicationTypeId;
								$scope.promoCodeData.promoCode = $scope.promoCodeData.promoCode;
								$scope.promoCodeData.isActive = $scope.promoCodeData.isActive;
								$scope.promoCodeData.description = $scope.promoCodeData.description;
								//$scope.promoCodeData.applicationTypeId = $scope.promoCodeData.applications[0].applicationTypeId;
							}

							$scope.generatePromoCodeManually = function() {
								$scope.promoCodeData.id = $scope.promoCodeData.id;
								$scope.promoCodeData.applicationTypeId = $scope.promoCodeData.applicationTypeId;
								$scope.promoCodeData.promoCode = $scope.promoCodeData.promoCode;
								$scope.promoCodeData.isActive = $scope.promoCodeData.isActive;
								$scope.promoCodeData.description = $scope.promoCodeData.description;
								
								promocodesService.generateFreshCouponsManually($scope.promoCodeData).then(
									function(success) {
										if (success.data.status == 200) {
											Notification.success(success.data.message);
											$scope.promoCodes=success.data.data;
											$scope.resetPromoCode();
										} else if (success.data.status == 400) {
											Notification.warning(success.data.message);
										} else {
											Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
										}
									}, function(error) {
										console.log("error==>>", error);
										$rootScope.validateErrorResponse(error);
									});
							}


						} ]);