app.service("promocodesService", ["httpService", "URLS", "$rootScope", "$http",
		function(httpService, URLS, $rootScope, $http) {

			this.getAllPromoCodes = function() {
				return httpService.get(URLS.user + "/promovouchers/getAllPromoCodes");
			};

			this.generateFreshCoupons = function(quantity, length, validDays) {
				return httpService.post(URLS.user + "/promovouchers/generateFreshCoupons/" + quantity+ "/" + length+ "/" + validDays);
			};
			
			this.generateFreshCouponsManually = function(data) {
				return httpService.post(URLS.user + "/promovouchers/generateFreshCouponsManually/", data);
			};

		} ]);