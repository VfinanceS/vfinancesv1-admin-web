app.service("payoutService", [ "httpService", "URLS", "$rootScope", "$http",
		function(httpService, URLS, $rootScope, $http) {

			this.getAllPayouts = function() {
				return httpService.get(URLS.user + "/payout/getAllPayouts");
			};
			
			this.savePayoutsFor = function(data) {
				return httpService.post(URLS.user + "/payout/savePayoutsFor", data);
			};
			
			this.getPaymentDetails = function(userId) {
				return httpService.get(URLS.user + "/payout/getPaymentDetailsForUser/"+userId);
			};
			
			this.savePayoutsForChnlPartner = function(data, chnlPrtnerId) {
				return httpService.post(URLS.user + "/payout/savePayoutsForChnlPrtnr/"+chnlPrtnerId, data);
			};
			
			this.getCpCommisionCalculationList = function(userId) {
				return httpService.get(URLS.user + "/payout/getCpCommisionCalculationList/"+userId);
			};
			
		} ]);