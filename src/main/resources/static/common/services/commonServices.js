app.factory('Notification', ["toastr", function (toastr) {

        var notification = {};

        notification.info = function (message) {
            
                toastr.info(message, 'Information', {
                    closeButton: true,
                    timeOut: 5000
                });
            
        };

        notification.confirm = function (message) {
            
                toastr.info('<button type="button" class="btn btn-primary">Yes</button><button type="button" class="btn btn-danger">No</button>', message, {allowHtml: true, timeOut: 0});
            
        };

        notification.warning = function (message) {
            
                toastr.warning(message, 'Warning', {
                    closeButton: true,
                    timeOut: 5000
                });
            
        };

        notification.error = function (message) {
            
                toastr.error(message, 'Error', {
                    closeButton: true,
                    timeOut: 5000
                });
            
        };

        notification.success = function (message) {
            
                toastr.success(message, {
                    closeButton: true,
                    timeOut: 5000
                });
            
        };
        
        return notification;
    }]);


app.service("masterService", [ 'httpService','URLS',"$http",
	function(httpService, URLS, $http) {

		this.pingRequest = function() {
			return httpService.get(URLS.user + "/ping");
		};

		this.countries = function(mode) {
			return httpService.get(URLS.user + '/master/get_country/'  + mode);
		};
		
		this.salutations = function(mode) {
			return httpService.get(URLS.user + '/master/get_salutation/'  + mode);
		};
		
		this.states = function(countryId) {
			return httpService.get(URLS.user + '/master/get_state_by_country_id/' + countryId);
		};
		
		this.cities = function(stateId) {
			return httpService.get(URLS.user + '/master/get_city_by_state_id/' + stateId);
		};
		
		this.banks = function(mode) {
			return httpService.get(URLS.user + '/master/get_banks/' + mode);
		};
		
		this.applicationType = function(mode) {
			return httpService.get(URLS.user + '/master/get_application_type/' + mode);
		};
		
	} ]);

app.service("reportService", [ 'httpService','URLS',"$http",
	function(httpService, URLS, $http) {

		this.pingRequest = function() {
			return httpService.get(URLS.user + "/ping");
		};

		this.getReportForAdmin = function() {
			return httpService.get(URLS.user + '/report/getAdminReport/');
		};
		
	} ]);

app.service("bankService", [ 'httpService','URLS',"$http",
   	function(httpService, URLS, $http) {

   		this.pingRequest = function() {
   			return httpService.get(URLS.user + "/ping");
   		};

   		this.getAllBankListForAdmin = function() {
   			return httpService.get(URLS.user + '/bank/getAllBanks/');
   		};
   		
   	} ]);

app.service("loanTypesService", [ 'httpService','URLS',"$http",
   	function(httpService, URLS, $http) {

   		this.pingRequest = function() {
   			return httpService.get(URLS.user + "/ping");
   		};

   		this.getAllLoanTypesForAdmin = function() {
   			return httpService.get(URLS.user + '/master/get_application_type/-1');
   		};
   		
   		this.updateLoanTypeDetails = function(data) {
   			return httpService.post(URLS.user + '/master/update_loan_type/',data);
   		};
   		
   	} ]);

app.service("documentService", [ 'httpService', 'URLS', "$http",
	function(httpService, URLS, $http) {

		this.getDocumentList = function(applicationId, data) {
			return httpService.post(URLS.user + "/getDocuments/" + applicationId, data);
		};

		this.getUserDocument = function(documentId) {
			return httpService.get(URLS.user + "/getUserDocuments/" + documentId);
		};
		
		this.getSelectedUserDocument = function(selectedUserId, documentId) {
			return httpService.get(URLS.user + "/getUserDocuments/" + selectedUserId+ "/" + documentId);
		};
		
		this.getReportDocuments = function(data) {
			return httpService.get(URLS.user + "/getReportDocuments/" );
		};

		this.inActiveDocument = function(documentMappingId) {
			return httpService.get(URLS.user + "/inActiveDocument/" + documentMappingId);
		};

		this.getCoApplicantsDocumentList = function(applicationId, coApplicantId, data) {
			return httpService.post(URLS.user + "/getCoApplicantsDocuments/" + applicationId + "/" + coApplicantId,data);
		};
		
		this.getPdfFile1 = function(docId){
	        return httpService.get(URLS.user + "/downloadpdf/"+docId, {responseType: 'arraybuffer'});
		};
		
		this.getPdfFile = function (docId) {
			return httpService.get(URLS.user + "/downloadpdf/"+docId
              , {
				responseType: 'arraybuffer',
				params: {
					//Required params
				},
			}).then(function (response, status, headers, config) {
				return response;
			});
		};
		
		this.getPdfFile = function (docId) {
            return httpService.get('/downloadpdf/'+docId, { responseType: 'arraybuffer' }).then(function (response) {
                return response;
            });
		};
		
		this.getPDReportingDocumentsList = function(applicationId, reportType,  data) {
			return httpService.post(URLS.user + "/getPDReportingDocumentsList/" + applicationId + "/"+reportType, data);
		};

	} ]); 



app.service("applicationService", [ 'httpService', 'URLS', "$rootScope", "$http",
	function(httpService, URLS, $rootScope, $http) {

		this.getLoanDetails = function(id, appTypeId) {
			return httpService.get(URLS.user + "/application/getLoanDetails/" + id + "/" + appTypeId);
		};
		
		this.getConnectionByLenderId = function(id) {
			return httpService.get(URLS.user + "/application/get_connection_by/" + id );
		};
		
		this.getConnections = function(appId,status) {
			return httpService.get(URLS.user + "/application/get_connections/" + appId + "/" + status);
		};

		this.updateStatus = function(appId) {
			return httpService.post(URLS.user + "/application/update_status_request",appId);
		};
		
		this.getDocumentsListForProcessing = function(applId) {
			return httpService.get(URLS.user + "/getDocumentsListForProcessing/"+applId);
		};
		
		this.disbursmentSaved = function(appId, disbursmentDate, disbursmentAmount) {
			var data = {};
			data.data = JSON.stringify({"applId":appId, "disbursmentDate":disbursmentDate, "disbursmentAmt":disbursmentAmount});
			return httpService.post(URLS.user + "/application/save_disbursment", data.data);
		};
		
		this.getDisbursmentList = function(id) {
			return httpService.get(URLS.user + "/application/getLoanDisbursmentDetails/" + id );
		};
		
		this.getApplicationsForPDReports = function(pduserId) {
			return httpService.get(URLS.user + "/application/get_applications_for_pdusers/"+pduserId);
		};
	} ]);

app.service("ServicePDF", [ 'httpService', 'URLS', "$rootScope", "$http",
	function(httpService, URLS, $rootScope, $http) {
		return {
	        downloadPdf: function (docId) {
	        return $http.get(URLS.user +'/downloadpdf/'+docId, { responseType: 'arraybuffer' }).then(function (response) {
	            return response;
	        });
	    }
    };
		
} ]);

app.factory('ServicePDF1', function ($http) {
    return {
        downloadPdf: function (docId) {
        return $http.get(URLS.user +'/downloadpdf/'+docId, { responseType: 'arraybuffer' }).then(function (response) {
            return response;
        });
    }
    };
});