app.service("userService", [ "httpService", "URLS", "$rootScope","$http",
		function(httpService, URLS, $rootScope, $http) {

			this.getUserDetailsById = function(userId) {
				return httpService.get(URLS.user + '/get_user_details_by_id/' + userId);
			};
	
			this.pingRequest = function() {
				return httpService.get(URLS.user + "/ping");
			};

			this.register = function(data) {
				return httpService.post(URLS.user + "/registration", data);
			};
			
			this.inviteLender = function(data) {
				return httpService.post(URLS.user + "/invite_lender", data);
			};
			
			this.updateLenderDetails = function(data) {
				return httpService.post(URLS.user + "/update_lender_details", data);
			};
			
			this.updatePDAgencyDetails = function(data) {
				return httpService.post(URLS.user + "/update_pd_agency_details", data);
			};
			
			this.updateUserConfigurationDetail = function(data) {
				return httpService.post(URLS.user + "/update_pd_agency_configuration", data);
			};
			
			this.updateInterviewQuestionsDetail = function(data) {
				return httpService.post(URLS.user + "/update_interview_questions", data);
			};
			
			this.updateFCIDetails = function(data) {
				return httpService.post(URLS.user + "/update_fci_agency_details", data);
			};
			
			this.updateAdminDetails = function(data) {
				return httpService.post(URLS.user + "/update_admin_details", data);
			};
			
			this.getLoggedInUserDetail = function() {
				return httpService.get(URLS.user + '/get_user_details');
			};
			
			this.getUserDetailById = function(id) {
				return httpService.get(URLS.user + '/get_user_details/' + id);
			};

			this.login = function(data) {
				return httpService.post(URLS.user + "/login", data);
			};
			
			this.getUserByType = function(userType) {
				return httpService.get(URLS.user + "/getUsersByType/" + userType);
			};
			
			this.getUserConfigByType = function(userType) {
				return httpService.get(URLS.user + "/getUsersConfigByType/" + userType);
			};
			
			this.getUserConfigByUser = function(userId) {
				return httpService.get(URLS.user + "/getUsersConfigByUser/" + userId);
			};
			
			this.getQuestions = function(userType) {
				return httpService.get(URLS.user + "/getAllQuestionByAgencyType/" + userType);
			};
			
			this.getUserConfig = function(userType) {
				return httpService.get(URLS.user + "/getUsersConfig/" + userType);
			};
			
			this.logout = function() {
				return httpService.get(URLS.user + "/logout");
			};
			
			this.getCpUsers = function(userType,userId) {
				return httpService.get(URLS.user + '/get_cp_users/' + userType + "/" + userId);
			};
			
			this.getBrCoApplicantUserDetails = function(brCoapplicantUserId){
				return httpService.get(URLS.user + '/get_br_coappl_user_details/' + brCoapplicantUserId);
			}
			
			this.changePassword = function(data) {
				return httpService.post(URLS.user + '/change_password', data);
			};
			
			this.updateBankDetails = function(data) {
				return httpService.post(URLS.user + "/bank/update_bank_details", data);
			};
			
			this.getQuestionsAnswers = function(applId) {
				return httpService.get(URLS.user + "/getAllQuestionAns/" + applId);
			};
			
		} ]);