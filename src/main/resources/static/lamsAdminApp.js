/**
 * ROUTER CONFIGURATION
 */
var app = angular.module("lamsAdmin", [ "ui.router", 
	"oc.lazyLoad",
	"ngCookies",
	"ngMessages",
	"toastr",
	"ui.bootstrap",
	"angular-loading-bar",
	"ngTable"
	]);

getUrls().then(bootstrapApplication);
function getUrls() {
	var initInjector = angular.injector([ "ng" ]);
	var $http = initInjector.get("$http");
	return $http.get("web/get_urls").then(function(response) {
		app.constant("URLS", response.data);
	}, function(errorResponse) {
		console.log("Something went wrong")
	});
}
function bootstrapApplication() {
	angular.element(document).ready(function() {
		angular.bootstrap(document, [ "lamsAdmin" ]);
	});
}

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.latencyThreshold = 500;
}]);

//app.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
//	$ocLazyLoadProvider.config({
//		  debug: true
//		});
//}]);

app.config([ "$stateProvider", "$urlRouterProvider", "$locationProvider", "$sceDelegateProvider",
	function($stateProvider, $urlRouterProvider, $locationProvider, $sceDelegateProvider, $controllerProvider) {
	console.log("calling login url......");
	$stateProvider
			.state("login", {
				url : '/login',
				templateUrl : 'common/htmls/login.html',
				controller : 'loginCtrl',
				data : {
					pageTitle : "Admin | Login"
				}
			})
			.state("admin.lams", {
				url : '/lams',
				abstract : true,
				views : {
					'header@admin' : {
						templateUrl : 'common/htmls/header.html',
					},
					'footer@admin' : {
						templateUrl : 'common/htmls/footer.html',
					},
					'sidebar@admin' : {
						templateUrl : 'common/htmls/sidebar.html',
					}
				}
			}).state("admin", {
			url : '/admin',
			templateUrl : 'admin.html',
		}).state("admin.lams.dashboard", {
			url : '/dashboard',
			views : {
				'content@admin' : {
					templateUrl : 'dashboard/dashboard.html',
					controller : 'dashboardCtrl'		
				}
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'dashboard/dashboardCtrl.js']});
                }]
			},
			data : {
				pageTitle : "Admin | Dashboard"
			}
		}).state("admin.lams.lenderProfile", {
	    	url : '/lenderProfile/:ldId',
	    	views :  {
	    		'content@admin' :  {
	    			templateUrl : 'profiles/ldProfile.html',
	        		controller: 'ldProfileCtrl'
	    		}
	    	},
	    	data : {pageTitle : "Admin | Lender Profile"},
	    	resolve: {
	            lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
	                return $ocLazyLoad.load({files: [
	                        'profiles/ldProfileCtrl.js']});
	            }]
	    	}
	   }).state("admin.lams.pdUserProfile", {
	    	url : '/pdProfile/:ldId',
	    	views :  {
	    		'content@admin' :  {
	    			templateUrl : 'profiles/pdProfile.html',
	        		controller: 'pdProfileCtrl'
	    		}
	    	},
	    	data : {pageTitle : "Admin | PD Profile"},
	    	resolve: {
	            lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
	                return $ocLazyLoad.load({files: [
	                        'profiles/pdProfileCtrl.js']});
	            }]
	    	}
	   }).state("admin.lams.lenders", {
			url : '/lenders',
			views : {
				'content@admin' : {
					templateUrl : 'usermanagement/lenders.html',
					controller : 'lendersCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Lenders"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'usermanagement/lendersCtrl.js']});
                }]
			}
		}).state("admin.lams.borrowerProfile", {
	    	url : '/borrwerProfile/:brId',
	    	views :  {
	    		'content@admin' :  {
	    			templateUrl : 'profiles/borrowerProfile.html',
	        		controller: 'borrowerProfileCtrl'
	    		}
	    	},
	    	data : {pageTitle : "Admin | Borrower Profile"},
	    	resolve: {
	            lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
	                return $ocLazyLoad.load({files: [
	                        'profiles/borrowerProfile.js']});
	            }]
	    	}
	   }).state("admin.lams.borrowers", {
			url : '/borrowers',
			views : {
				'content@admin' : {
					templateUrl : 'usermanagement/borrowers.html',
					controller : 'borrowersCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Borrowers"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'usermanagement/borrowersCtrl.js']});
                }]
			}
		}).state("admin.lams.channelPartners", {
			url : '/channelPartners',
			views : {
				'content@admin' : {
					templateUrl : 'usermanagement/channelPartners.html',
					controller : 'channelPartnerCtrl'
				}
			},
			data : {
				pageTitle : "Admin | channel Partners"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'usermanagement/channelPartnersCtrl.js']});
                }]
			}
		}).state("admin.lams.channelPartnersPayouts", {
			url : '/channelPartnersPayouts',
			views : {
				'content@admin' : {
					templateUrl : 'usermanagement/channelPartnersPayouts.html',
					controller : 'channelPartnersPayoutsCtrl'
				}
			},
			data : {
				pageTitle : "Admin | channel Partners Payouts"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'usermanagement/channelPartnersPayoutsCtrl.js']});
                }]
			}
		}).state("admin.lams.adminProfile", {
			url : '/adminprofiles',
			views : {
				'content@admin' : {
					templateUrl : 'profile/adminProfile.html',
					controller : 'adminProfileCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Profiles"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'profile/adminProfileCtrl.js']});
                }]
			}
		}).state("admin.lams.changePassword", {
			url : '/changepassword',
			views : {
				'content@admin' : {
					templateUrl : 'profile/changePassword.html',
					controller : 'changePasswordCtrl'
				}
			},
			data : {
				pageTitle : "Lams | Change Password"
			},
	    	resolve: {
	            lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
	                return $ocLazyLoad.load({files: [
	                        'profile/changePasswordCtrl.js']});
	            }]
			}
		}).state("admin.lams.Report", {
			url : '/report',
			views : {
				'content@admin' : {
					templateUrl : 'reports/reports.html',
					controller : 'reportsCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Reports"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'reports/reportsCtrl.js']});
                }]
			}
		}).state("admin.lams.Banks", {
			url : '/banks',
			views : {
				'content@admin' : {
					templateUrl : 'bank/banks.html',
					controller : 'banksCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Banks Management"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'bank/banksCtrl.js']});
                }]
			}
		}).state("admin.lams.LoanTypes", {
			url : '/loantypes',
			views : {
				'content@admin' : {
					templateUrl : 'loantypes/loantypes.html',
					controller : 'loanTypesCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Loan Types Management"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'loantypes/loanTypesCtrl.js']});
                }]
			}
		}).state("admin.lams.pdUserMgmt", {
			url : '/pdusers',
			views : {
				'content@admin' : {
					templateUrl : 'pdusers/pdusermgmt.html',
					controller : 'pdUsermgmtCtrl'
				}
			},
			data : {
				pageTitle : "Admin | PD Users Management"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'pdusers/pdUsermgmtCtrl.js']});
                }]
			}
		}).state("admin.lams.pdUserconfig", {
			url : '/pdusersconfig',
			views : {
				'content@admin' : {
					templateUrl : 'pdusers/pduserconfig.html',
					controller : 'pdUserConfigCtrl'
				}
			},
			data : {
				pageTitle : "Admin | PD Users Configuration"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'pdusers/pdUserConfigCtrl.js']});
                }]
			}
		}).state("admin.lams.promoCodesMgmt", {
			url : '/promocodes',
			views : {
				'content@admin' : {
					templateUrl : 'promocodes/promocodesmgmt.html',
					controller : 'promoCodesMgmtCtrl'
				}
			},
			data : {
				pageTitle : "Admin | Promo Codes Management"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'promocodes/promoCodesMgmtCtrl.js']});
                }]
			}
		}).state("admin.lams.fciUserMgmt", {
			url : '/fciusers',
			views : {
				'content@admin' : {
					templateUrl : 'fciusers/fciusermgmt.html',
					controller : 'fciUsermgmtCtrl'
				}
			},
			data : {
				pageTitle : "Admin | FCI Users Management"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'fciusers/fciUsermgmtCtrl.js']});
                }]
			}
		}).state("admin.lams.fciUserConfig", {
			url : '/fciusersconfig',
			views : {
				'content@admin' : {
					templateUrl : 'fciusers/fciuserconfig.html',
					controller : 'fciUserConfigCtrl'
				}
			},
			data : {
				pageTitle : "Admin | FCI Users Configuration"
			},
			resolve: {
                lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({files: [
                            'fciusers/fciUserConfigCtrl.js']});
                }]
			}
		}).state("admin.lams.channelPartnerProfile", {
	    	url : '/channelPartnerProfile/:cpId',
	    	views :  {
	    		'content@admin' :  {
	    			templateUrl : 'profiles/channelPartnerProfile.html',
	        		controller: 'channelPartnerProfileCtrl'
	    		}
	    	},
	    	data : {pageTitle : "Admin | Channel Partner Profile"},
	    	resolve: {
	            lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
	                return $ocLazyLoad.load({files: [
	                        'profiles/channelPartnerProfileCtrl.js']});
	            }]
	    	}
	   }).state("admin.lams.application", {
	    	url : '/application/:appCode/:appId/:empType',
	    	views :  {
	    		'content@admin' :  {
	    			templateUrl : 'profiles/application.html',
	        		controller: 'applicationCtrl'
	    		}
	    	},
	    	data : {pageTitle : "Admin | Borrower Application Details"},
	    	resolve: {
	            lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
	                return $ocLazyLoad.load({files: [
	                        'profiles/applicationCtrl.js']});
	            }]
	    	}
	   });
	
		$urlRouterProvider.otherwise("login");
		$locationProvider.hashPrefix(''); 
		$locationProvider.html5Mode(true);
	} ]);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        if ($rootScope.activeCalls == undefined) {
            $rootScope.activeCalls = 0;
        }
//        console.log("$rootScope.activeCalls====>",$rootScope.activeCalls);

        return {
            request: function (config) {
                $rootScope.activeCalls += 1;
                return config;
            },
            requestError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            },
            response: function (response) {
                $rootScope.activeCalls -= 1;
                return response;
            },
            responseError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            }
        };
    });
}]);

	//app.config(['$stateProvider', '$httpProvider', '$locationProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider',
	//	function ($stateProvider, $httpProvider, $locationProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider) {
	//
	//		app.controller = $controllerProvider.register;
	//		app.directive = $compileProvider.directive;
	//		app.filter = $filterProvider.register;
	//		app.factory = $provide.factory;
	//		app.service = $provide.service;
	//		app.constant = $provide.constant;
	//		app.value = $provide.value;
	//		
	//	}]);