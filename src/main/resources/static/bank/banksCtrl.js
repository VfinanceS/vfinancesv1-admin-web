angular.module("lamsAdmin").controller("banksCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification","NgTableParams","$filter","bankService","documentService",
	function($scope, $http, $rootScope, Constant, userService, Notification, NgTableParams, $filter, bankService,documentService) {

		$scope.allBankList = [];
		$scope.userData = {};
		
		$scope.getAllBankListForAdmin = function() {
			bankService.getAllBankListForAdmin().then(
				function(success) {
					if (success.data.status == 200) {
						$scope.allBankList = success.data.data;
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getAllBankListForAdmin();
		
		$scope.updateBankDetails = function() {
			if (!$scope.forms.bankForm.$valid) {
				$scope.forms.bankForm.$submitted = true;
				Notification.warning("Please fill all mandatory data");
				return false;
			}
			
			console.log("$scope.userData===>",$scope.userData);
			userService.updateBankDetails($scope.userData).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.success(success.data.message);
						$scope.allBankList.push(success.data.data);
					} else if (success.data.status == 400) {
						Notification.warning(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.editBankData = function(bank) {
			$scope.showEditMode = true;
			$scope.userData = angular.copy(bank);
		}
		
	} ]);