angular.module("lamsAdmin").controller("fciUsermgmtCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification", "NgTableParams", "$filter",
	function($scope, $http, $rootScope, Constant, userService, Notification,NgTableParams, $filter) {

		$scope.forms = {};
		$scope.users = [];
		$scope.resetUser = function(){
			$scope.userData = {bank : {}};
		}
		$scope.getUsers = function(userType) {
			//userType is NUll then will fetch all the users
			userService.getUserByType(userType).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.users = success.data.data;
						$scope.userTable.reload();
	                    $scope.userTable.page(1);						
						console.log("$scope.users==>", $scope.users);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getUsers(Constant.UserType.FCI_USER.id);

		$scope.editUserData = function(user) {
			$scope.userData = angular.copy(user);
			$scope.showEditMode = true;
			$scope.userData.password = $scope.userData.tempPassword;
			$scope.userData.confirmPassword = $scope.userData.tempPassword;
			if(!$rootScope.isEmpty($scope.userData.applications)){
				$scope.userData.applicationTypeId = $scope.userData.applications[0].applicationTypeId;				
			}
			$("#userEmail").focus();
		}

		$scope.updateFCIAgencyDetails = function() {
			if (!$scope.forms.fciAgencyForm.$valid) {
				$scope.forms.fciAgencyForm.$submitted = true;
				Notification.warning("Please fill all mandatory data");
				return false;
			}
			if ($scope.userData.password.trim() != $scope.userData.confirmPassword.trim()) {
				Notification.warning("Password and confirm passwod not matched!!");
				return false;
			}
			$scope.userData.userType = Constant.UserType.FCI_USER.id;
			$scope.userData.applications = [{"applicationTypeId" : $scope.userData.applicationTypeId}];
			$scope.userData.isProfileFilled = true;
			console.log("$scope.userData===>",$scope.userData);
			userService.updateFCIAgencyDetails($scope.userData).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.success(success.data.message);
						$scope.resetUser();
						$scope.getUsers(Constant.UserType.FCI_USER.id);
					} else if (success.data.status == 400) {
						Notification.warning(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}

		$scope.search = {};
		$scope.$watch("search.fciuser", function () {
            $scope.userTable.reload();
            $scope.userTable.page(1);
        });
		
		$scope.userTable = new NgTableParams({page: 1, count: 500, sorting: {firstName: "asc"}}, {
            counts: [],
            getData: function ($defer, params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.users, params.orderBy()) : $scope.users;
                if ($scope.search.fciuser) {
                    orderedData = $filter('filter')(orderedData, $scope.search.fciuser);
                }
                if (orderedData) {
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            }
        });

		
		

	} ]);