angular.module("lamsAdmin").controller("loanTypesCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification","NgTableParams","$filter","loanTypesService","documentService",
	function($scope, $http, $rootScope, Constant, userService, Notification, NgTableParams, $filter, loanTypesService,documentService) {

		$scope.allLoanTypesList = [];
		$scope.userData = {};
		
		$scope.getAllLoanTypesForAdmin = function() {
			loanTypesService.getAllLoanTypesForAdmin().then(
				function(success) {
					if (success.data.status == 200) {
						$scope.allLoanTypesList = success.data.data;
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getAllLoanTypesForAdmin();
		
		$scope.updateLoanTypeDetails = function() {
			if (!$scope.forms.LoanTypeForm.$valid) {
				$scope.forms.LoanTypeForm.$submitted = true;
				Notification.warning("Please fill all mandatory data");
				return false;
			}
			
			console.log("$scope.userData===>",$scope.userData);
			loanTypesService.updateLoanTypeDetails($scope.userData).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.success(success.data.message);
						$scope.allLoanTypesList.push(success.data.data);
					} else if (success.data.status == 400) {
						Notification.warning(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.editLoanTypeData = function(loanType) {
			$scope.showEditMode = true;
			$scope.userData = angular.copy(loanType);
		}
		
	} ]);