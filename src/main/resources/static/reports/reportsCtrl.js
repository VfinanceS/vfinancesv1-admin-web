angular.module("lamsAdmin").controller("reportsCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification","NgTableParams","$filter","reportService","documentService",
	function($scope, $http, $rootScope, Constant, userService, Notification, NgTableParams, $filter, reportService,documentService) {

		$scope.reportDataList = [];
		$scope.getReportForAdmin = function() {
			reportService.getReportForAdmin().then(
				function(success) {
					if (success.data.status == 200) {
						$scope.reportDataList = success.data.data;
						$scope.reportDocumentList([ Constant.documentType.REPORT_DOCUMENT]);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getReportForAdmin();
		
		$scope.reportDocumentList = [];
		$scope.reportDocumentList = function(listOfDocumentMstId) {
			documentService.getReportDocuments(listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.reportDocumentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
	} ]);