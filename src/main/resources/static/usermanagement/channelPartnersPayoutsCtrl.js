angular
		.module("lamsAdmin")
		.controller(
				"channelPartnersPayoutsCtrl",
				[
						"$scope",
						"$http",
						"$rootScope",
						"Constant",
						"userService",
						"Notification",
						"NgTableParams",
						"$filter",
						"payoutService",
						function($scope, $http, $rootScope, Constant,
								userService, Notification, NgTableParams,
								$filter, payoutService) {

							$scope.IsAllCategoriesCollapsed = false;
							$scope.allPayOuts = [];
							$scope.getAllPayOuts = function() {
								payoutService
										.getAllPayouts()
										.then(
												function(success) {
													if (success.data.status == 200) {
														$scope.allPayOuts = success.data.data;
													} else if (success.data.status == 400) {
														Notification.error(success.data.message);
													} else {
														Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
													}
												},
												function(error) {
													console.log("error==>>",error);
													$rootScope.validateErrorResponse(error);
												});
							}
							$scope.getAllPayOuts();

							$scope.addNew = function(index) {
								$scope.allPayOuts[index].payOuts.push({
									"id":null,
									"applicationTypeId":null,
									"disbursmentFromAmt":0,
									"disbursmentToAmt":0,
									"payOutInPerc":0,
									"version":null,
									"createdDate":null,
									"createdBy":null,
									"modifiedDate":null,
									"modifiedBy":null,
									"channelPartners":null
								});
							};
							
							$scope.addNew = function(index) {
								console.log("Index : "+index+" element : "+$scope.allPayOuts[0]);
								$scope.allPayOuts[index].payOuts.push({
									"id":null,
									"applicationTypeId":null,
									"disbursmentFromAmt":0,
									"disbursmentToAmt":0,
									"payOutInPerc":0,
									"version":null,
									"createdDate":null,
									"createdBy":null,
									"modifiedDate":null,
									"modifiedBy":null,
									"channelPartners":null
								});
							};
							
							$scope.savePayoutFor = function(index) {
								
								var applWithPayData = {};
								applWithPayData.id = $scope.allPayOuts[index].id;
								applWithPayData.code = $scope.allPayOuts[index].code;
								applWithPayData.name = $scope.allPayOuts[index].name;
								applWithPayData.payOuts = [];
								
								$scope.validationFlag = false;
								
								$scope.allPayOuts[index].payOuts.forEach(function (row) {
									applWithPayData.payOuts.push(row);
						        });
								
								angular.forEach(applWithPayData.payOuts,
										function(row) {
											if((row.disbursmentFromAmt >= 0 && row.disbursmentToAmt > 0 && row.payOutInPerc > 0)){
												$scope.validationFlag = true;
											}
											else{
												Notification.warning("Please fill all mandatory fields");
												$scope.validationFlag = false;
											}
										});
								
								applWithPayData.data = JSON.stringify(applWithPayData.payOuts);
								
								if($scope.validationFlag){
									payoutService
									.savePayoutsFor(applWithPayData)
									.then(
											function(success) {
												if (success.data.status == 200) {
													Notification.info("Payouts Saved  Successfully!!");
												} else if (success.data.status == 400) {
													Notification.error(success.data.message);
												} else {
													Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
												}
											},
											function(error) {
												console.log("error==>>",error);
												$rootScope.validateErrorResponse(error);
											});
								}
							};

							$scope.remove = function(selectedRow, parentInx, indx) {
								var newDataList = [];
								$scope.selectedAll = false;

								$scope.allPayOuts[parentInx].payOuts.splice(indx, 1);
								
								$scope.personalDetails = newDataList;
							};
							
							$scope.collapseAll = function() {
								$scope.IsAllCategoriesCollapsed = !$scope.IsAllCategoriesCollapsed;
							    $scope.allPayOuts.forEach(function(item) {
							    	item.isCollapsed = $scope.IsAllCollapsed;
							    })
							}
							
							$scope.expandAll = function(expanded) {
						        // $scope is required here, hence the injection above, even though we're using "controller as" syntax
						        $scope.$broadcast('onExpandAll', {
						          expanded: expanded
						        });
						      };

							$scope.$on('onExpandAll',function(event,args){
								$scope.expanded = args.expanded;
						    })
						    
							$scope.checkAll = function() {
								if (!$scope.selectedAll) {
									$scope.selectedAll = true;
								} else {
									$scope.selectedAll = false;
								}
								angular.forEach($scope.personalDetails,
												function(personalDetail) {
													personalDetail.selected = $scope.selectedAll;
												});
								
								angular.forEach($scope.allPayOuts,
										function(payout) {
											payout.selected = $scope.selectedAll;
										});
							};
							
							$scope.expandAll = function(expanded) {
						        // $scope is required here, hence the injection
								// above, even though we're using "controller
								// as" syntax
						        $scope.$broadcast('onExpandAll', {
						          expanded: expanded
						        });
						      };

						} ]);